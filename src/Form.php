<?php

namespace OpenForms;

use OpenForms\ApiClient;
use OpenForms\Component;
use OpenForms\Panel;
use OpenForms\ApiClient;
use GuzzleHttp\Client;
use http\Url;
use Throwable;

class Form {

    private $form;
    private $components;
    private $display;
    private $page;
    private $numPages;

    public function __construct($formUri)
    {
        $this->headers = [
            'Accept'        => 'application/ld+json',
            'Content-Type'  => 'application/json',
        ];

        $this->components = [];
        $this->display = "form"
        $this->page = 0;
        $this->numPages = false;


        // If a form uri was passed we want to use the api client to get that form
        if($formUri){

        }
    }

    /**
     * This function sends mail from id-vault to provided receiver
     *
     * @param string $applicationId id of your id-vault application.
     * @param string $body html body of the mail.
     * @param string $subject subject of the mail.
     * @param string $receiver receiver of the mail.
     * @param string $sender sender of the mail.
     *
     * @return array|false returns response from id-vault or false if wrong information provided for the call
     */
    public function sendMail(string $applicationId, string $body, string $subject, string $receiver, string $sender)
    {

        return $response;
    }



    /* @todo een add component functie, die kijk of de component al in de components array hangt en zo niet deze toevoegd */

    /* @todo een remove component functie, die kijkt of de component in de components array hangt en zo ja deze verwijderd */

    /* @todo een to html weergave */

}
