<?php

namespace OpenForms;

use OpenForms\Component;
use GuzzleHttp\Client;
use http\Url;
use Throwable;

class Panel {

    private $type;
    private $input;
    private $title;
    private $theme;
    private $components;
    private $clearOnHide;
    private $tableView;
    private $hideLabel;

    public function __construct($formUri)
    {
        $this->type = "type"
        $this->input = false
        $this->title = "page";
        $this->theme = "default";
        $this->components = [];
        $this->clearOnHide = false
        $this->tableView = false
        $this->hideLabel = false
    }


    /* @todo write an populate form array method */
    public function fromArray(array $array) Component
    {
        // do some magic
        return $this;
    }

    /* @todo write an function to translate the object to an array */
    public function toArray() Component
    {
        $array = [];
        // do some magic
        return $array;
    }


    /* @todo add getters and setters */

    /**
     *
     * @return Component
     */
    public function setLabel(string $label) Component
    {
        $this->label = $label;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getLabel(string $label) string
    {
        return $this->label;
    }

    /* @todo een add component functie, die kijk of de component al in de components array hangt en zo niet deze toevoegd */

    /* @todo een remove component functie, die kijkt of de component in de components array hangt en zo ja deze verwijderd */

    /* @todo een to html weergave */


}
