<?php

namespace OpenForms;

use OpenForms\ApiClient;
use GuzzleHttp\Client;
use http\Url;
use Throwable;

class Component {

    /* @todo set the component properties

    components need to have te following public properties
    input: true,
    tableView: true,
    inputType: "text",
    inputMask: "",
    label: "First Name",
    key: "firstName",
    placeholder: "Enter your first name",
    prefix: "",
    suffix: "",
    multiple: false,
    defaultValue: "",
    protected: false,
    unique: false,
    persistent: true,
    -validate: {
    required: false,
    minLength: "",
    maxLength: "",
    pattern: "",
    custom: "",
    customPrivate: false
    },
    -conditional: {
    show: false,
    when: null,
    eq: ""
    },
    type: "textfield",
    $$hashKey: "object:249",
    autofocus: false,
    hidden: false,
    clearOnHide: true,
    spellcheck: true

    */


    public function __construct($formUri)
    {
        /* @todo set deafult values */
        $this->proptertyName = "default value"
    }

    /* @todo write an populate form array method */
    public function fromArray(array $array) Component
    {
        // do some magic
        return $this;
    }

    /* @todo write an function to translate the object to an array */
    public function toArray() Component
    {
        $array = [];
        // do some magic
        return $array;
    }


    /* @todo add getters and setters */

    /**
     *
     * @return Component
     */
    public function setLabel(string $label) Component
    {
        $this->label = $label;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getLabel(string $label) string
    {
        return $this->label;
    }

    /* @todo een to html functie voor een boorstrap weergave van de component */

}
