<?php

namespace OpenForms;

use GuzzleHttp\Client;
use http\Url;
use Throwable;

class ApiClient {
    /**
     * Endpoint of the API
     */
    const API_ENDPOINT = 'https://open-forms.com';

    /**
     * HTTP Methods
     */
    const HTTP_GET = 'GET';
    const HTTP_POST = 'POST';

    private $client;
    private $headers;

    public function __construct()
    {

        $this->headers = [
            'Accept'        => 'application/ld+json',
            'Content-Type'  => 'application/json',
        ];

        $this->client = new Client([
            'headers'  => $this->headers,
            'base_uri' => self::API_ENDPOINT,
            'timeout'  => 20.0,
        ]);

    }
}
